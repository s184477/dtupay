package org.acme;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CustomerResource {

    private final CustomerService customerService;

    public CustomerResource(CustomerService customerService) {
        this.customerService = customerService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomer(String cid) {
        if (cid == null || cid.trim().isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Invalid customer id")
                    .build();
        }

        if (customerService.customerExist(cid)) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Customer with id " + cid + " already exists")
                    .build();
        }

        Customer customer = new Customer();
        customer.setCid(cid);

        customerService.createCustomer(customer);

        return Response.status(Response.Status.CREATED).build();
    }

}
