package org.acme;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("/payment")
public class PaymentResource {
    PaymentService ps;
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Payment payments(){
        return new Payment("mid1","cid1",10, UUID.randomUUID().toString());
    }

    @GET
    @Path("Payments")
    public List<Payment> getPayments(){
       return ps.getPayments();
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPayment(Payment p) {
        if(p.getAmount() < 0){
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid Payment amount").build();
        }
        if(!ps.customerExist(p.getCid())){
            return Response.status(Response.Status.BAD_REQUEST).entity("CID does not exist").build();
        }
        if(!ps.customerExist(p.getMid())){
            return Response.status(Response.Status.BAD_REQUEST).entity("MID does not exist").build();
        }
        ps.createPayment(p.getCid(),p.getMid(),p.getAmount(),p.getId());
        return Response.status(Response.Status.OK).build();
    }
    @POST
    @Path("customer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerCustomer(String cid){
        if(ps.addCustomer(cid)){
            return Response.status(Response.Status.BAD_REQUEST).entity("CID already exist").build();
        }
        ps.addCustomer(cid);
        return Response.status(Response.Status.CREATED).entity("Customer created with CID:" + " " + cid ).build();
    }

    @POST
    @Path("merchant")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registerMerchant(String mid){
        if(ps.addMerchant(mid)){
            return Response.status(Response.Status.BAD_REQUEST).entity("mid already exist").build();
        }
        ps.addMerchant(mid);
        return Response.status(Response.Status.CREATED).entity("merchant created with mid:" + " " + mid ).build();
    }
}
