package org.acme;

import java.util.ArrayList;
import java.util.List;

public class MerchantService {

    private List<String> Merchants;

    public MerchantService() {
        this.Merchants = new ArrayList<String>();
    }

    public void createMerchant(Merchant merchant) {
        Merchants.add(merchant.getMid());
    }

    public boolean customerExist(String cid){
        return Merchants.contains(cid);
    }
}
