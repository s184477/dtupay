package org.acme;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerService {
    private List<String> customers;

    public CustomerService() {
        this.customers = new ArrayList<String>();
    }

    public void createCustomer(Customer customer) {
        customers.add(customer.getCid());
    }

    public boolean customerExist(String cid){
        return customers.contains(cid);
    }
}
