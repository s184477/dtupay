package org.acme;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class MerchantResource {

    List<Merchant> merchants;
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMerchant(String mid) {
        return Response.status(Response.Status.CREATED).build();
    }
}
