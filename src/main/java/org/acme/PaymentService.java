package org.acme;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PaymentService {
    private List<Payment> paymentList = new ArrayList<>();
   private List<Customer> customerList = new ArrayList<>();

    private List<Merchant> merchantList = new ArrayList<>();

    Payment payment = new Payment();

    public Payment getPayment() {
        return payment;
    }

    public List<Payment> getPayments() {
        return paymentList;
    }


    public boolean customerExist(String cid){
        return customerList.stream().anyMatch(customer -> Objects.equals(customer.cid, cid));
    }
    public boolean merchantExist(String mid){
        return merchantList.stream().anyMatch(Merchant -> Objects.equals(Merchant.mid, mid));
    }
    public boolean addCustomer(String cid){
        if(customerExist(cid)){
            return false;
        }
        Customer c = new Customer(cid);
        customerList.add(c);
        return true;
    }

    public boolean addMerchant(String mid){
        if(merchantExist(mid)){
            return false;
        }
        Merchant m = new Merchant(mid);
        merchantList.add(m);
        return true;
    }
    public void createPayment(String cid, String mid,int amount,String id) {
        paymentList.add(new Payment(cid,mid,amount,id));
    }
}
